const { ModuleFederationPlugin } = require('webpack').container;
const { REMOTE_URL = 'http://localhost:3002' } = process.env;

const webpackConfigPath = 'react-scripts/config/webpack.config';
const webpackConfig = require(webpackConfigPath);

const override = (config) => {
  config.plugins.push(new ModuleFederationPlugin(require('../../modulefederation.config.js')));

  // config.output.libraryTarget = 'umd';
  // config.output.path = '/federated-host-app';
  config.output.publicPath = 'auto';

  return config;
};

require.cache[require.resolve(webpackConfigPath)].exports = (env) => override(webpackConfig(env));

module.exports = require(webpackConfigPath);
