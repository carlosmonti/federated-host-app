const { dependencies } = require('./package.json');
const { REMOTE_URL = 'http://localhost:3002' } = process.env;

module.exports = {
  name: 'federatedHost',
  remotes: {
    federatedLibrary: `federatedLibrary@${REMOTE_URL}/federated-component-library/remoteEntry.js`,
  },
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
