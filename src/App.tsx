import React from 'react';
import logo from './logo.svg';
import './App.css';

const Button = React.lazy(
  () => import('federatedLibrary/Button')
);

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <React.Suspense fallback={<div>Loading...</div>}>
          <Button>My Federated Button</Button>
        </React.Suspense>
      </header>
    </div>
  );
}

export default App;
