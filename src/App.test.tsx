import { render, screen } from '@testing-library/react';
import { act } from "react-dom/test-utils";
import App from './App';

jest.mock('federatedLibrary/Button', () => ({ ...props }) => <button type='button' {...props} />, { virtual: true });

test('renders <App />', async () => {
  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(() => {
    render(<App />);
  });

  const linkElement = screen.getByText(/My Federated Button/i);
  expect(linkElement).toBeInTheDocument();
});
