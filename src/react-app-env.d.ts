/// <reference types="react-scripts" />

declare module "federatedLibrary/Button" {
	const Button: React.ButtonHTMLAttributes;

	export default Button;
}

declare module "federatedLibrary/CSSReset" {
	const CSSReset: React.ComponentType;

	export default CSSReset;
}
